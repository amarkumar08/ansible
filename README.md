Ansible Installation process on ubuntu:
python is installed on both the machines i.e Master and Slave, by default AWS:
# sudo apt-get install python3
Enable SSH access between Ansible Master and Slave.
On master:
# ssh-keygen
# sudo cat ./.ssh/id_rsa.pub
Copy the output of this file, and paste it in the slave machine’s authorized_keys file. You can do that using the following command. Once inside the file, paste the output:
# sudo nano ./.ssh/authorized_keys
Insert the new entry in the second line of the file.
# INSTALLING ANSIBLE ON MASTER:
# sudo apt update
# sudo apt install software-properties-common
# sudo apt-add-repository --yes --update ppa:ansible/ansible
# sudo apt install ansible
